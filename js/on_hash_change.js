function onHashChange(params) {
    var hastag = window.location.hash.substr(1)
    var xhr = typeof XMLHttpRequest != 'undefined' ? new XMLHttpRequest() : new ActiveXObject('Microsoft.XMLHTTP');
    xhr.open('get', params+'/'+hastag+'.html', true);
    xhr.onreadystatechange = function() {
        if (xhr.readyState == 4 && xhr.status == 200) { 
            document.getElementById("datadiri").innerHTML = xhr.responseText;
        } 
    }
    xhr.send();
}